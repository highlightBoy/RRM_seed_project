//
//  RootViewController.h
//  RESideMenuExample
//
//  Created by zhouyi on 14-8-5.
//  Copyright (c) 2014年 Roman Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVTabBar.h"
#import "RDVTabBarItem.h"
#import "RDVTabBarController.h"
@interface RootViewController : UIViewController<UIGestureRecognizerDelegate>

@property (strong, nonatomic) RDVTabBarController *viewController;


+(RootViewController*)shareInstance;
- (RDVTabBarController*)setupViewControllers;
@end
