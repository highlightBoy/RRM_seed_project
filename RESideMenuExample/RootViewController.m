//
//  RootViewController.m
//  RESideMenuExample
//
//  Created by zhouyi on 14-8-5.
//  Copyright (c) 2014年 Roman Efimov. All rights reserved.
//

#import "RootViewController.h"
#import "DEMOContentFirstViewController.h"
#import "DEMOContentSecondViewController.h"
#import "DEMOContentThirdViewController.h"
#import "MLNavigationController.h"
@interface RootViewController ()

@end

@implementation RootViewController

static RootViewController *__defaultController;
static RDVTabBarController *__tabBarController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)___cominit{
//    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]init];
//    [self.view addGestureRecognizer:pan];
//    [self setupViewControllers];
}


+(RootViewController*)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        RootViewController *root = [[RootViewController alloc]init];
        __defaultController = root;
    });
    return __defaultController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (RDVTabBarController*)setupViewControllers {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        DEMOContentFirstViewController *firstViewController = [[DEMOContentFirstViewController alloc] init];
        MLNavigationController *firstNavigationController = [[MLNavigationController alloc]
                                                             initWithRootViewController:firstViewController];
//        UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(__panGestureRecognized:)];
//        panGestureRecognizer.delegate = self;
//        [firstViewController.view addGestureRecognizer:panGestureRecognizer];
        
        DEMOContentSecondViewController *secondViewController = [[DEMOContentSecondViewController alloc] init];
        MLNavigationController *secondNavigationController = [[MLNavigationController alloc]
                                                              initWithRootViewController:secondViewController];
        
        DEMOContentThirdViewController *thirdViewController = [[DEMOContentThirdViewController alloc] init];
        MLNavigationController *thirdNavigationController = [[MLNavigationController alloc]
                                                             initWithRootViewController:thirdViewController];
        
        
        RDVTabBarController *tabBarController = [[RDVTabBarController alloc] init];
        [tabBarController setViewControllers:@[firstNavigationController, secondNavigationController,thirdNavigationController]];
        [tabBarController.view setBackgroundColor:[UIColor clearColor]];
        
        //    self.viewController = tabBarController;
        __tabBarController = tabBarController;
        [self customizeTabBarForController:tabBarController];
    });
    return __tabBarController;
}

- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController {
    UIImage *finishedImage = [UIImage imageNamed:@"tabbar_selected_background"];
    UIImage *unfinishedImage = [UIImage imageNamed:@"tabbar_normal_background"];
    NSArray *tabBarItemImages = @[@"first", @"second", @"third"];
    
    NSInteger index = 0;
    for (RDVTabBarItem *item in [[tabBarController tabBar] items]) {
        [item setBackgroundSelectedImage:finishedImage withUnselectedImage:unfinishedImage];
        UIImage *selectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",
                                                      [tabBarItemImages objectAtIndex:index]]];
        UIImage *unselectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",
                                                        [tabBarItemImages objectAtIndex:index]]];
        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
        
        index++;
    }
    
    //    [tabBarController tabBar].backgroundView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:0.9];
}

- (void)customizeInterface {
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    
    UIImage *backgroundImage = nil;
    NSDictionary *textAttributes = nil;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        backgroundImage = [UIImage imageNamed:@"navigationbar_background_tall"];
        
        textAttributes = @{
                           NSFontAttributeName: [UIFont boldSystemFontOfSize:18],
                           NSForegroundColorAttributeName: [UIColor blackColor],
                           };
    } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
        backgroundImage = [UIImage imageNamed:@"navigationbar_background"];
        
        textAttributes = @{
                           UITextAttributeFont: [UIFont boldSystemFontOfSize:18],
                           UITextAttributeTextColor: [UIColor blackColor],
                           UITextAttributeTextShadowColor: [UIColor clearColor],
                           UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetZero],
                           };
#endif
    }
    
    [navigationBarAppearance setBackgroundImage:backgroundImage
                                  forBarMetrics:UIBarMetricsDefault];
    [navigationBarAppearance setTitleTextAttributes:textAttributes];
}

-(void)setRDvTabBarHiddenYES:(id)sender{
    
    [self.viewController setTabBarHidden:YES];
    
    
}
-(void)setRDvTabBarHiddenNO:(id)sender{
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.viewController setTabBarHidden:NO];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
