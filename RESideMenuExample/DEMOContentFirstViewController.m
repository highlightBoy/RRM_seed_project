//
//  DEMOContentFirstViewController.m
//  RESideMenuExample
//
//  Created by zhouyi on 14-8-5.
//  Copyright (c) 2014年 Roman Efimov. All rights reserved.
//

#import "DEMOContentFirstViewController.h"
#import "DEMOFourthViewController.h"
@interface DEMOContentFirstViewController ()

@end

@implementation DEMOContentFirstViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"First Content Controller";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Left"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(presentLeftMenuViewController:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Right"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(presentRightMenuViewController:)];
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(showLeftMenuViewControllerWithPan:)];
    [self.view addGestureRecognizer:pan];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)push:(id)sender {
    DEMOFourthViewController *a= [[DEMOFourthViewController alloc]init];
    [self.navigationController pushViewController:a animated:YES];
    
    
}
@end
