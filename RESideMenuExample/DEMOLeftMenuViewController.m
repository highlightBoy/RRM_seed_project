//
//  DEMOMenuViewController.m
//  RESideMenuExample
//
//  Created by Roman Efimov on 10/10/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOLeftMenuViewController.h"
#import "RootViewController.h"
#import "DEMOSecondViewController.h"
#import "MLNavigationController.h"
@interface DEMOLeftMenuViewController ()

@property (strong, readwrite, nonatomic) UITableView *tableView;

@end

@implementation DEMOLeftMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 130, self.view.frame.size.width, 48 * 5) style:UITableViewStylePlain];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.bounces = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
    [self setHeaderView];
    [self setFooterView];
}
-(void)setHeaderView{
    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 120)];
    [header setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:header];
    
    UIImageView *avatar = [[UIImageView alloc]initWithFrame:CGRectMake(20, 60, 64, 64)];
    [avatar setImage:[UIImage imageNamed:@"avatar"]];
    avatar.layer.masksToBounds =YES;
    avatar.layer.cornerRadius = 32;
    [header addSubview:avatar];
    
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(105, 60, 120, 44)];
    [name setText:@"自然美"];
    [name setTextColor:[UIColor whiteColor]];
    [header addSubview:name];
    
    UIButton *icon_heart = [[UIButton alloc]initWithFrame:CGRectMake(110, 102, 14, 14)];
    [icon_heart setBackgroundImage:[UIImage imageNamed:@"icon_zan"] forState:UIControlStateNormal];
    [header addSubview:icon_heart];
    
    self.title_heart = [[UILabel alloc]initWithFrame:CGRectMake(133, 100 , 25, 18)];
    self.title_heart.font = [UIFont systemFontOfSize:14];
    [self.title_heart setText:@"45"];
    [self.title_heart setTextColor:[UIColor whiteColor]];
    [header addSubview:self.title_heart];
    
    UIButton *icon_cart = [[UIButton alloc]initWithFrame:CGRectMake(160, 104, 14, 12)];
    [icon_cart setBackgroundImage:[UIImage imageNamed:@"icon_cart"] forState:UIControlStateNormal];
    [header addSubview:icon_cart];
    
    self.title_cart = [[UILabel alloc]initWithFrame:CGRectMake(179, 100, 25, 18)];
    self.title_cart.font = [UIFont systemFontOfSize:14];
    [self.title_cart setText:@"12"];
    [self.title_cart setTextColor:[UIColor whiteColor]];
    [header addSubview:self.title_cart];
    
    
    
}
-(void)setFooterView{
    UIView *footer = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-40, self.view.frame.size.width, 40)];
    [footer setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:footer];
    
    UIButton *icon_signOut = [[UIButton alloc]initWithFrame:CGRectMake(20, 0, 18, 14)];
    [icon_signOut setBackgroundImage:[UIImage imageNamed:@"icon_cart"] forState:UIControlStateNormal];
    [footer addSubview:icon_signOut];
    
    UIButton *title_signOut = [[UIButton alloc]initWithFrame:CGRectMake(20,0, 90, 14)];
    [title_signOut setTitle:@"注销" forState:UIControlStateNormal];
    [footer addSubview:title_signOut];
    
    
    
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self.sideMenuViewController setContentViewController:[[RootViewController shareInstance] setupViewControllers]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 1:
        {
            DEMOSecondViewController *secondViewController = [[DEMOSecondViewController alloc] init];
            MLNavigationController *secondNavigationController = [[MLNavigationController alloc]
                                                                  initWithRootViewController:secondViewController];
            [self.sideMenuViewController setContentViewController:secondNavigationController
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        }
        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    
    NSArray *titles = @[@"首页", @"日历", @"我的收藏", @"设置"];
    NSArray *images = @[@"IconHome", @"IconCalendar", @"IconProfile", @"IconSettings", @"IconEmpty"];
    cell.textLabel.text = titles[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
    
    return cell;
}

@end
